package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    private static Connection connection;
    private static final Lock CONNECTION_LOCK = new ReentrantLock();

    private static final String FIND_ALL_USERS = "SELECT * FROM users";
    private static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
    private static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT,?)";
    private static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT,?)";
    private static final String DELETE_USER = "DELETE FROM users WHERE login = ?";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
    private static final String GET_USER = "SELECT * FROM users WHERE login = ?";
    private static final String GET_TEAM = "SELECT * FROM teams WHERE name = ?";
    private static final String FIND_USER_TEAMS = "SELECT t.id, t.name FROM users_teams ut\n" +
            "JOIN users u ON ut.user_id = u.id\n" +
            "JOIN teams t ON ut.team_id = t.id\n" +
            "WHERE u.id = ?";
    private static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES (?,?)";
    private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        String URL = "";
        try (InputStream inputStream = new FileInputStream("app.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);
            URL = properties.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(URL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Statement st = connection.createStatement();
             ResultSet rs = st.executeQuery(FIND_ALL_USERS)) {
            CONNECTION_LOCK.lock();
            while (rs.next()) {
                User user = User.createUser(rs.getString("login"));
                user.setId(rs.getInt("id"));
                users.add(user);
            }

        } catch (SQLException e) {
            System.out.println("Can't find all users:" + e.getMessage());
            throw new DBException("Can't find all users:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (PreparedStatement ps = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            CONNECTION_LOCK.lock();
            ps.setString(1, user.getLogin());
            if (ps.executeUpdate() != 1) {
                return false;
            }
            try (ResultSet id = ps.getGeneratedKeys()) {
                if (id.next()) {
                    int idField = id.getInt(1);
                    user.setId(idField);
                }
            }
        } catch (SQLException e) {
            System.out.println("Can't insert user:" + e.getMessage());
            throw new DBException("Can't insert user:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
        return true;
    }


    public User getUser(String login) throws DBException {
        User user = null;
        try (PreparedStatement ps = connection.prepareStatement(GET_USER)) {
            CONNECTION_LOCK.lock();
            ps.setString(1, login);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    user = User.createUser(rs.getString("login"));
                    user.setId(rs.getInt("id"));
                }
            }
        } catch (SQLException e) {
            System.out.println("Can't get user:" + e.getMessage());
            throw new DBException("Can't get user:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        try (PreparedStatement ps = connection.prepareStatement(GET_TEAM)) {
            CONNECTION_LOCK.lock();
            ps.setString(1, name);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    team = Team.createTeam(rs.getString("name"));
                    team.setId(rs.getInt("id"));
                }
            }
        } catch (SQLException e) {
            System.out.println("Can't get team:" + e.getMessage());
            throw new DBException("Can't get team:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }

        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Statement st = connection.createStatement();
             ResultSet rs = st.executeQuery(FIND_ALL_TEAMS)) {
            CONNECTION_LOCK.lock();
            while (rs.next()) {
                Team team = Team.createTeam(rs.getString("name"));
                team.setId(rs.getInt("id"));
                teams.add(team);
            }
        } catch (SQLException e) {
            System.out.println("Can't find all team:" + e.getMessage());
            throw new DBException("Can't find all team:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (PreparedStatement ps = connection.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            CONNECTION_LOCK.lock();
            ps.setString(1, team.getName());
            if (ps.executeUpdate() != 1) {
                return false;
            }
            try (ResultSet id = ps.getGeneratedKeys()) {
                if (id.next()) {
                    int idField = id.getInt(1);
                    team.setId(idField);
                }
            }
        } catch (SQLException e) {
            System.out.println("Can't insert team:" + e.getMessage());
            throw new DBException("Can't insert team:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try (PreparedStatement ps = connection.prepareStatement(SET_TEAMS_FOR_USER)) {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            for (Team team : teams) {
                ps.setInt(1, user.getId());
                ps.setInt(2, team.getId());
                ps.addBatch();
            }
            int[] usersGroups = ps.executeBatch();
            for (int i : usersGroups) {
                if (i != 1) {
                    return false;
                }
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e2) {
                e2.printStackTrace();
            }
            System.out.println("Can't set teams for user:" + e.getMessage());
            throw new DBException("Can't set teams for user:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_USER)) {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            for (User user : users) {
                ps.setString(1, user.getLogin());
                ps.addBatch();
            }
            int[] usersGroups = ps.executeBatch();
            for (int i : usersGroups) {
                if (i != 1) {
                    return false;
                }
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e2) {
                e2.printStackTrace();
            }
            System.out.println("Can't delete user:" + e.getMessage());
            throw new DBException("Can't delete user:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(FIND_USER_TEAMS)) {
            CONNECTION_LOCK.lock();
            ps.setInt(1, user.getId());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Team team = Team.createTeam(rs.getString("name"));
                    team.setId(rs.getInt("id"));
                    teams.add(team);
                }
            }
        } catch (SQLException e) {
            System.out.println("Can't find all teams for user:" + e.getMessage());
            throw new DBException("Can't find all teams for user:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_TEAM)) {
            CONNECTION_LOCK.lock();
            ps.setString(1, team.getName());
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Can't delete team:" + e.getMessage());
            throw new DBException("Can't delete team:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_TEAM)) {
            CONNECTION_LOCK.lock();
            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            System.out.println("Can't update team:" + e.getMessage());
            throw new DBException("Can't update team:" + e.getMessage(), e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
    }

}
